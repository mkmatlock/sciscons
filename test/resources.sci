{
'local': {'host': 'localhost',
          'executor':'local',
          'queues': {'short_1': ('00:15:00', 1, 0),
                     'short_2': ('00:15:00', 1, 0),
                     'short_3': ('00:15:00', 1, 0),
                     'short_4': ('00:15:00', 1, 0)}},
'chpc': {'host': 'login.chpc.wustl.edu',
         'user': 'matlockm',
         'executor':'pbs',
         'workingdir': '/scratch/matlockm',
         'queues': {'pe1_4h': ('04:00:00', 1, 1),
                    'pe1_24h': ('24:00:00', 1, 1),
                    'pe1_168h': ('168:00:00', 1, 1)}
}
