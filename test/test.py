import os
import unittest
from sciscons import Env
from sciscons.setup import load_setup_options, initialize_sciscons, source_virtualenv

class SciScons_TestSuite(unittest.TestCase):
    def setUp(self):
        initialize_sciscons()
        self.setup_options = load_setup_options()
        source_virtualenv( self.setup_options )

    def test_completer(self):
        env = Env(self.setup_options, mode='complete')
        execfile('SConstruct', {'env':env})
        self.assertEquals( 'F4\nF5\nF7\nF8\n', env.output.getvalue() )

    def test_lister(self):
        env = Env(self.setup_options, mode='list')
        execfile('SConstruct', {'env':env})
        self.assertEquals( '------------------\nDefined Endpoints:\n------------------\nF4\nF5\nF7\nF8\n------------------\n', env.output.getvalue() )

    def test_build_graph(self):
        env = Env(self.setup_options, mode='visualize', output_file='.sciscons/dependencies.html')
        execfile('SConstruct', {'env':env})
        self.assertEquals( '', env.output.getvalue() )

        self.assertTrue( os.path.exists('.sciscons/tipsy.js') )
        self.assertTrue( os.path.exists('.sciscons/tipsy.css') )
        self.assertTrue( os.path.exists('.sciscons/sciscons.js') )
        self.assertTrue( os.path.exists('.sciscons/sciscons.css') )
        with open(env.output_file, 'r') as ifile:
            html = ifile.read()

        self.assertTrue( 'make_F3' in html )
        self.assertTrue( 'make_F4' in html )
        self.assertTrue( 'make_F5' in html )
        self.assertTrue( 'make_F6' in html )
        self.assertTrue( 'make_F7' in html )
        self.assertTrue( 'make_F8' in html )
        self.assertTrue( 'make_more_stuff.scons' in html )
        self.assertTrue( 'sort -r $SOURCE &#62; $TARGET' in html )

    def test_verify_graph(self):
        env = Env(self.setup_options, mode='verify')
        execfile('SConstruct', {'env':env})
        self.assertEquals( 'No errors or warnings found for project: Test Project\n', env.output.getvalue() )
