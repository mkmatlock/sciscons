import sys

if __name__=='__main__':
    ch = {}

    contents = open(sys.argv[1], 'r').read()
    for c in contents:
        v = ch.get(c, 0)
        ch[c] = v+1

    for c in sorted(ch):
        print c, ch[c]
