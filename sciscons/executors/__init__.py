class Resources(object):
    def __init__(self, time, cores=1, gpus=0):
        self.h, self.m, self.s = [int(t) for t in time.split(':')]
        self.cores = cores
        self.gpus = gpus

class Executor(object):
    def build(self):
        pass

    def execute(self):
        pass
