import os, subprocess, shutil
from contextlib import contextmanager
from pkg_resources import resource_filename

DEFAULT_VIRTUALENV_DIR = 'venv'
DEFAULT_MODULES_DIR = 'modules'
FNULL = open(os.devnull, 'w')

@contextmanager
def workingdir( path ):
    cwd = os.getcwd()
    os.chdir( path )
    yield
    os.chdir( cwd )

def touch(fname, times=None):
    with open(fname, 'a'):
        os.utime(fname, times)

def copy_and_replace(fsrc, fdest, replacements):
    with open(fsrc, 'r') as srcfile:
        with open(fdest, 'w') as destfile:
            for line in srcfile:
                for search in replacements:
                    repl = replacements[search]
                    line = line.replace(search, repl)
                destfile.write(line)

def initialize_sciscons():
    if not os.path.exists(".sciscons"):
        os.mkdir(".sciscons")

def create_project( project_name ):
    os.mkdir( project_name )

    os.mkdir( os.path.join( project_name, 'scripts' ) )
    os.mkdir( os.path.join( project_name, 'scratch' ) )
    os.mkdir( os.path.join( project_name, 'scons' ) )
    os.mkdir( os.path.join( project_name, 'output' ) )
    os.mkdir( os.path.join( project_name, 'raw_data' ) )
    os.mkdir( os.path.join( project_name, 'data' ) )
    os.mkdir( os.path.join( project_name, 'figure' ) )
    os.mkdir( os.path.join( project_name, 'results' ) )
    os.mkdir( os.path.join( project_name, 'modules' ) )

    touch( os.path.join( project_name, 'scripts', '__init__.py' ) )
    shutil.copy( resource_filename('sciscons.default_project', 'SConstruct'), os.path.join( project_name, 'SConstruct' ) )
    shutil.copy( resource_filename('sciscons.default_project', 'scons_procedures.py'), os.path.join( project_name, 'scons_procedures.py' ) )

    replacements = { "PROJECT_NAME": project_name,
                     "VENV_DIR": "%s_venv" % (project_name),
                     "REMOTE_USERNAME": os.environ.get('SCISCONS_REMOTE_USERNAME', os.environ['USER']) }
    copy_and_replace( resource_filename('sciscons.default_project', 'setup.sci'), os.path.join( project_name, 'setup.sci' ), replacements )
    copy_and_replace( resource_filename('sciscons.default_project', '.hgignore'), os.path.join( project_name, '.hgignore' ), replacements )
    copy_and_replace( resource_filename('sciscons.default_project', 'rscons.config'), os.path.join( project_name, 'rscons.config' ), replacements )

def load_setup_options():
    try:
        with open("setup.sci", "r") as reqfile:
            setup_options = eval( reqfile.read() )
            return setup_options
    except Exception, e:
        raise IOError("Error parsing setup.sci: %s" % (e.message))

def get_project_name( setup_options ):
    return setup_options.get('project_name', os.path.dirname( os.getcwd() ))

def setup_sciscons( use_system_packages=False, clean=False, setup_options={} ):
    virtual_env_dir = setup_options.get('virtual_env_name', DEFAULT_VIRTUALENV_DIR)
    requires = setup_options.get('requirements', {'hg':[], 'pip':[]})
    use_virtual_env = setup_options.get('use_virtual_env', True)

    if clean:
        if os.path.exists( virtual_env_dir ):
            shutil.rmtree( virtual_env_dir, ignore_errors=True )

        if os.path.exists( DEFAULT_MODULES_DIR ):
            shutil.rmtree( DEFAULT_MODULES_DIR, ignore_errors=True )

    if use_virtual_env:
        if os.path.exists( os.path.join( virtual_env_dir, 'bin', 'activate' ) ):
            print "Using existing virtualenv: ", virtual_env_dir
        else:
            print "Creating virtualenv at:", virtual_env_dir
            setup_virtualenv( virtual_env_dir, use_system_packages )

        source_virtualenv( setup_options )

    install_default_modules( use_virtual_env )
    if 'pip' in requires:
        install_pip_modules( requires['pip'], use_virtual_env )
    if 'hg' in requires:
        install_hg_modules( requires['hg'], use_virtual_env )

def setup_virtualenv( virtual_env_dir, use_system_packages ):
    if os.path.exists( virtual_env_dir ):
        raise Exception( "Virtual env already exists at: " + virtual_env_dir )

    venv_args = "--no-site-packages"
    if use_system_packages:
        venv_args = "--system-site-packages"

    subprocess.check_call([ "virtualenv", venv_args, virtual_env_dir ])

def source_virtualenv( setup_options ):
    if not setup_options.get( 'use_virtual_env', True ):
        return

    virtual_env_dir = setup_options.get( 'virtual_env_name', DEFAULT_VIRTUALENV_DIR )

    path_to_activate = os.path.join( virtual_env_dir, 'bin', 'activate_this.py' )
    execfile(path_to_activate, dict(__file__=path_to_activate))

def install_default_modules( use_virtual_env ):
    if use_virtual_env:
        subprocess.check_call([ "pip", "install", "--upgrade", "pip" ])

def install_hg_modules( modules, use_virtual_env ):
    if os.path.exists( DEFAULT_MODULES_DIR ):
        shutil.rmtree( DEFAULT_MODULES_DIR )
    os.mkdir( DEFAULT_MODULES_DIR )

    mod_path = os.path.abspath( DEFAULT_MODULES_DIR )

    for location, rev, postprocess in modules:
        name = location[location.rfind('/')+1:]
        target_loc = os.path.join( mod_path, name )

        if rev is None or rev.strip() == '':
            print "Cloning:", location
            subprocess.check_call([ 'hg', 'clone', location, target_loc ])
        else:
            print "Cloning:", location, rev
            subprocess.check_call([ 'hg', 'clone', '-r', rev, location, target_loc ])

        if postprocess == 'setup':
            if not use_virtual_env:
                raise Exception("Cannot install compiled modules without use_virtual_env=True in setup.sci")

            print "Installing:", name
            with workingdir( target_loc ):
                subprocess.check_call([ "python", "setup.py", "install" ])
            shutil.rmtree( target_loc )

def install_pip_modules( modules, use_virtual_env ):
    import pip
    for mod, version in modules:
        if not use_virtual_env:
            raise Exception("Cannot install pip modules without use_virtual_env=True in setup.sci")
        print "Install: %s%s" % (mod, version)
        pip.main([ "install", mod+version ])
