import sys

if __name__=='__main__':
    args = sys.argv[1:]

    invert=False
    if args[0] == '-i':
        invert=True
        args = args[1:]

    input_file = args[0]
    line_set = set( args[1:] )
    SEP = '\t'

    with open(input_file, 'rU') as ifile:
        i=0
        for line in ifile:
            if i > 0:
                ID = line[ :line.find( SEP ) ]
                if invert != ( ID in line_set ):
                    sys.stdout.write( line )
            else:
                sys.stdout.write( line )
            i+=1

