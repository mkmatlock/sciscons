import os
import re
import time
import hashlib
import socket

class File(object):
    def __init__(self, path, time, fhash, build, time_only=False):
        self.path = path
        self.time_mode = time_only
        self.time = time
        self.fhash = fhash
        self.build = build

    def hashsum( self ):
        BLOCKSIZE = 65536
        md5 = hashlib.md5()
        with open(self.path, 'rb') as ifile:
            buf = ifile.read(BLOCKSIZE)
            while len(buf) > 0:
                md5.update(buf)
                buf = ifile.read(BLOCKSIZE)
        return str(md5.hexdigest())

    def last_modified( self ):
        if os.path.exists( self.path ):
            return os.path.getmtime( self.path )
        return 0

    def changed( self ):
        mtime = os.path.getmtime( self.path )
        changed = mtime > self.ltime
        nhash = 0

        if not self.time_mode and changed:
            nhash = self.hashsum()
            changed = self.lhash != nhash
        return changed, mtime, nhash

def format_shell(targets, sources, shell):
    if "$SOURCES" in shell:
        shell = shell.replace('$SOURCES', " ".join(sources))
    if "$TARGETS" in shell:
        shell = shell.replace('$TARGETS', " ".join(targets))

    if "$SOURCE" in shell:
        shell = shell.replace('$SOURCE', sources[0])
    if "$TARGET" in shell:
        shell = shell.replace('$TARGET', targets[0])

    nshell = shell
    for m in re.finditer(r"${SOURCES\[([0-9]+)\]}", shell):
        nshell = nshell.replace(m.group(0), sources[int(m.group(1))])
    for m in re.finditer(r"${TARGETS\[([0-9]+)\]}", shell):
        nshell = nshell.replace(m.group(0), targets[int(m.group(1))])
    return nshell

class Command(object):
    def __init__(self, targets, sources, scripts, shell):
        self.targets = targets
        self.sources = sources
        self.scripts = scripts
        self.shell = shell

    def execute(self):
        self.node = socket.gethostname()
        self.start = time.time()

        # run command


        self.finish = time.time()

class Build(object):
    def __init__(self, number):
        self.number = number
        self.files = {}
        self.commands = {}

    def add_file( self, fobj ):
        self.files[fobj.path] = fobj

    def has_file( self, fobj ):
        return fobj.path in self.files

    def get_file( self, path ):
        return self.files[path]

