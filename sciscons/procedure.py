import json, base64
import string
from StringIO import StringIO
from pkg_resources import resource_filename
from inspect import stack
import imp
import os
import sys
from sciscons.setup import source_virtualenv, load_setup_options, get_project_name
from sciscons.html import visualize

SUCCESS_STATUS_CODE = 0
ERROR_STATUS_CODE = 1
NO_SUCH_EXPERIMENT_STATUS = 4
BP_STATUS_CODE = 8
UNFINISHED_STATUS_CODE = 16

STATUS_CODE_DEFS = {0: 'Success', 1: 'Error',
                    4: 'No Such Endpoint',
                    8: 'Continue'}

SCISCONS_DIR = ".sciscons"

PROCEDURE_ID_CODE = 0

def get_procedure_id():
    global PROCEDURE_ID_CODE
    PROCEDURE_ID_CODE += 1
    return PROCEDURE_ID_CODE

def find_module(name, path=None):
    for mod in name.split('.'):
        res = imp.find_module(mod, path)
        path = [res[1]]
    return path[0]

def resource_relative(module, resource, start=os.getcwd()):
    path = resource_filename(module, resource)
    return os.path.relpath(path, start)

def make_list( obj ):
    if isinstance(obj, tuple):
        obj = list(obj)
    if not isinstance(obj, list):
        obj = [ obj ]
    return obj

def encode_basic(O):
    S = json.dumps(O)
    B = base64.b64encode(S)
    return B

def decode_basic(B):
    S = base64.b64decode(B)
    O = json.loads(S)
    return O

class SafeDict(dict):
    def __missing__(self, key):
        return '{' + key + '}'

def do_replace_args( command, **kwargs):
    D = {
         's0': "${SOURCES[0]}",
         's1': "${SOURCES[1]}",
         's2': "${SOURCES[2]}",
         's3': "${SOURCES[3]}",
         's4': "${SOURCES[4]}",
         's5': "${SOURCES[5]}",
         't0': "${TARGETS[0]}",
         't1': "${TARGETS[1]}",
         't2': "${TARGETS[2]}",
         't3': "${TARGETS[3]}",
         't4': "${TARGETS[4]}",
         't5': "${TARGETS[5]}",

         's0sq': "'${SOURCES[0]}'",
         's1sq': "'${SOURCES[1]}'",
         's2sq': "'${SOURCES[2]}'",
         's3sq': "'${SOURCES[3]}'",
         's4sq': "'${SOURCES[4]}'",
         's5sq': "'${SOURCES[5]}'",
         't0sq': "'${TARGETS[0]}'",
         't1sq': "'${TARGETS[1]}'",
         't2sq': "'${TARGETS[2]}'",
         't3sq': "'${TARGETS[3]}'",
         't4sq': "'${TARGETS[4]}'",
         't5sq': "'${TARGETS[5]}'",

         's0dq': "\"${SOURCES[0]}\"",
         's1dq': "\"${SOURCES[1]}\"",
         's2dq': "\"${SOURCES[2]}\"",
         's3dq': "\"${SOURCES[3]}\"",
         's4dq': "\"${SOURCES[4]}\"",
         's5dq': "\"${SOURCES[5]}\"",
         't0dq': "\"${TARGETS[0]}\"",
         't1dq': "\"${TARGETS[1]}\"",
         't2dq': "\"${TARGETS[2]}\"",
         't3dq': "\"${TARGETS[3]}\"",
         't4dq': "\"${TARGETS[4]}\"",
         't5dq': "\"${TARGETS[5]}\"",
         }
    for k in kwargs:
        D[k] = kwargs[k]
    rep_dict = SafeDict(D)
    formatted_cmd = string.Formatter().vformat(command, (), rep_dict)
    return formatted_cmd

class Env(object):
    def __init__(self, setup_options, mode='run', step=1, verbosity=1, endpoint=None,
                 output_file=None, node_id=None, shell='/bin/bash'):
        self.__endpoints = {}

        self.shell=shell

        self.endpoint = endpoint
        self.node_id = node_id
        self.mode = mode
        self.step = step
        self.verbosity = verbosity
        self.status_code = UNFINISHED_STATUS_CODE
        self.output_file = output_file
        self.output = StringIO()

        if self.verbosity > 3:
            print "Python Path:", sys.path

        self.setup_options = setup_options
        sys.excepthook = self.handle_system_exception

    def set_status(self, code):
        self.status_code=status_code

    def handle_system_exception(self, type, value, traceback):
        try:
            self.status_code = ERROR_STATUS_CODE
        except:
            pass
        sys.__excepthook__(type, value, traceback)

    def run(self):
        if self.mode == 'list':
            print >>self.output, "------------------"
            print >>self.output, "Defined Endpoints:"
            print >>self.output, "------------------"
            for ep_name in sorted(self.list_endpoints()):
                print >>self.output, ep_name
            print >>self.output, "------------------"
        if self.mode == 'complete':
            for ep_name in sorted(self.list_endpoints()):
                print >>self.output, ep_name
        if self.mode == 'verify':
            if self.endpoint is not None and self.endpoint not in self.__endpoints:
                self.status_code = NO_SUCH_EXPERIMENT_STATUS
                raise Exception("No such endpoint: %s" % (self.endpoint))

            dependency_graph = visualize.DependencyAggregator()
            endpoint_list = self.get_endpoints( self.endpoint )
            procedure_list = self.flatten_procedure_graph( endpoint_list )
            for procedure in procedure_list:
                dependency_graph.run_procedure( procedure )

            dependency_graph.post_process()
            visualize.report_errors( self.output, dependency_graph, get_project_name( self.setup_options ) )

        if self.mode == 'visualize':
            if self.endpoint is not None and self.endpoint not in self.__endpoints:
                self.status_code = NO_SUCH_EXPERIMENT_STATUS
                raise Exception("No such endpoint: %s" % (self.endpoint))

            dependency_graph = visualize.DependencyAggregator()
            endpoint_list = self.get_endpoints( self.endpoint )
            procedure_list = self.flatten_procedure_graph( endpoint_list )
            for procedure in procedure_list:
                dependency_graph.run_procedure( procedure )

            dependency_graph.post_process()
            visualize.write_html( dependency_graph, get_project_name( self.setup_options ), self.output_file )

        if self.mode == 'run':
            if self.endpoint is not None and self.endpoint not in self.__endpoints:
                self.status_code = NO_SUCH_EXPERIMENT_STATUS
                raise Exception("No such endpoint: %s" % (self.endpoint))

            experiment_scope = Experiment()
            if self.node_id is not None:
                node = self.find_node( int( self.node_id ) )
                procedure_list = self.flatten_procedure_graph([ node ])
            else:
                endpoint_list = self.get_endpoints( self.endpoint )
                procedure_list = self.flatten_procedure_graph( endpoint_list )

            bp_count = 0
            for procedure in procedure_list:
                try:
                    if self.verbosity >= 4:
                        sys.stderr.write("RUNNING PROCEDURE:\n")
                        sys.stderr.write(repr(procedure)+"\n")

                    build = self.node_id is None or int(self.node_id)==procedure.procedure_id
                    if build:
                        experiment_scope.enable()
                    else:
                        experiment_scope.disable()
                    status_code = experiment_scope.run_procedure( procedure )
                    if self.verbosity >= 4:
                        sys.stderr.write("STATUS: %d\n" % (status_code))
                    if status_code == BP_STATUS_CODE:
                        bp_count+=1
                        if self.step <= bp_count:
                            if self.verbosity >= 1:
                                sys.stderr.write("Breakpoint reached")
                            os.system('echo "%d" > %s' % (BP_STATUS_CODE, STATUS_FILE))
                            return
                except Exception, e:
                    sys.stderr.write("Error occured while running procedure: %s\n%s\n" % (repr(procedure), repr(e)))
                    os.system('echo "%d" > %s' % (ERROR_STATUS_CODE, STATUS_FILE))
                    raise

            os.system('echo "%d" > %s' % (SUCCESS_STATUS_CODE, STATUS_FILE))

    def find_node(self, node_id):
        visited_nodes = set()
        next_nodes = sorted( self.__endpoints.values(), key=lambda p: p.procedure_id )

        while len( next_nodes ) > 0:
            procedure_node = next_nodes.pop( 0 )
            if procedure_node.procedure_id == node_id:
                return procedure_node

            if procedure_node.procedure_id in visited_nodes:
                continue
            visited_nodes.add( procedure_node.procedure_id )
            for parent_proc in procedure_node.parents:
                next_nodes.append( parent_proc )

        raise Exception("No such node ID: %d" % (node_id))

    def get_endpoints( self, initial_endpoint_name=None ):
        if initial_endpoint_name is None:
            endpoint_list = sorted( self.__endpoints.values(), key=lambda p: p.procedure_id )
        else:
            endpoint_list = [ self.__endpoints[initial_endpoint_name] ]

        return endpoint_list

    def flatten_procedure_graph( self, endpoint_list ):
        selected_subgraph = [ ]
        visited_nodes = set()
        next_nodes = [ e for e in endpoint_list ]

        while len( next_nodes ) > 0:
            procedure_node = next_nodes.pop( 0 )
            if procedure_node.procedure_id in visited_nodes:
                continue

            visited_nodes.add( procedure_node.procedure_id )
            selected_subgraph.append( procedure_node )

            for parent_proc in procedure_node.parents:
                next_nodes.append( parent_proc )


        S = [ p for p in selected_subgraph if len( p.parents ) == 0 ]
        L = []
        visited_edges = set()
        visited_nodes_2 = set()

        while len( S ) > 0:
            pn = S.pop(0)
            if pn.procedure_id not in visited_nodes_2:
                L.append(pn)
                visited_nodes_2.add(pn.procedure_id)

            for pc in pn.children:
                e = (pn.procedure_id, pc.procedure_id)
                if pc.procedure_id in visited_nodes:
                    visited_edges.add( e )

                    if all([ (pp.procedure_id, pc.procedure_id) in visited_edges for pp in pc.parents ]):
                        S.append(pc)

        return L

    def list_endpoints(self):
        return sorted([ name for name in self.__endpoints ])

    def create_endpoint( self, name ):
        endpoint = EndPoint(name)
        if endpoint.name in self.__endpoints:
            raise Exception("Endpoint already exists: %s" % (endpoint.name))
        self.__endpoints[endpoint.name] = endpoint
        return endpoint

    def get_endpoint( self, name ):
        if name not in self.__endpoints:
            raise Exception("No such endpoint: %s" % (name))
        return self.__endpoints[name]

class Experiment(object):
    def __init__( self ):
        self.files = {}
        self.dependencies = {}
        self.targets = {}
        self.current_procedure = None
        self.enabled = True

    def enable(self):
        self.enabled=True

    def disable(self):
        self.enabled=False

    def run_procedure( self, procedure ):
        self.current_procedure = procedure
        status_code = procedure.run( self )
        self.current_procedure = None
        return status_code

    def add_target( self, name, path ):
        if path in self.files:
            raise Exception("Error in %s, Path already defined as '%s': %s" % (self.current_procedure.name, name, path))
        if name in self.targets:
            raise Exception("Error in %s, Target already defined with name: %s" % (self.current_procedure.name, name))
        if name in self.dependencies:
            raise Exception("Error in %s, Dependency already defined with name: %s" % (self.current_procedure.name, name))

        self.targets[name] = path
        self.dependencies[name] = path
        self.files[path] = name
        return name

    def add_dependency( self, name, path ):
        if path in self.files:
            raise Exception("Error in %s, Path already defined as '%s': %s" % (self.current_procedure.name, self.files[path], path))
        if name in self.targets:
            raise Exception("Error in %s, Target already defined with name: %s" % (self.current_procedure.name, name))
        if name in self.dependencies:
            raise Exception("Error in %s, Dependency already defined with name: %s" % (self.current_procedure.name, name))

        self.dependencies[name] = path
        self.files[path] = name
        return name

    def shell( self, targets, sources, command, script=None, shell=False, exit=False ):
        source_files = self.__process_sources( sources )
        target_files = self.__process_targets( targets )

        if script != None:
            source_files = [script] + source_files

        if self.enabled:
            PyScript( target_files, source_files, command, shell=shell, exit=exit )

    def python( self, targets, sources, module, args=None, script=False, shell=False, exit=False, pyargs="" ):
        source_files = self.__process_sources( sources )
        target_files = self.__process_targets( targets )

        if script:
            working_dir = os.getcwd()
            path = find_module( module )
            if path.startswith( working_dir ):
                if working_dir.endswith( os.sep ):
                    path = path[ len(working_dir): ]
                else:
                    path = path[ len(working_dir)+1: ]
            source_files = [path] + source_files

        if args is None:
            args = "$SOURCES > $TARGET"
            if script:
                args = "${SOURCES[1:]} > $TARGET"

        if self.enabled:
            PyScript( target_files, source_files, "python %s -m %s %s" % (pyargs, module, args), shell=shell, exit=exit )

    def scons( self, targets, sources, scons_file, options="-Q", sig_options="", j=4, tempdir=None, exit=False, clean=True ):
        source_files = self.__process_sources( sources )
        target_files = self.__process_targets( targets )

        if self.enabled:
            PyScons( target_files, source_files, scons_file,
                    options=options+" -j %d" % (j), sig_options=sig_options,
                    tempdir=tempdir, exit=exit, clean=clean )

    def __get_target_path( self, name ):
        if name in self.targets:
            return self.targets[name]
        else:
            options = ", ".join( list(self.targets.keys()) )
            raise Exception("Error in %s, No such target: %s\nOptions: %s" % (self.current_procedure.name, name, options))

    def perform_target_access( self, names, access_func, default_value ):
        tpaths = [ self.get_target_path( n ) for n in names ]
        return access_func( *tpaths )

    def __process_targets( self, targets ):
        targets = make_list( targets )

        target_files = []
        for s in targets:
            if s in self.targets:
                target_files.append( self.targets[s] )
            else:
                options = ", ".join( list(self.targets.keys()) )
                raise Exception("Error in %s, No such target: %s\nOptions: %s" % (self.current_procedure.name, s, options))

        return target_files

    def __process_sources( self, sources ):
        sources = make_list( sources )

        source_files = []
        for s in sources:
            if s in self.dependencies:
                source_files.append( self.dependencies[s] )
            else:
                options = ", ".join( list(self.dependencies.keys()) + list(self.targets.keys()) )
                raise Exception("No such dependency or target: %s\nOptions: %s" % (s, options))

        return source_files

class SubScons(Experiment):
    def __init__(self):
        super(SubScons, self).__init__()
        self.env = DefaultEnvironment( ENV=os.environ,
                                  tools=['default', PYTOOL()],
                                  IMPLICIT_COMMAND_DEPENDENCIES=0,
                                  SHELL='/bin/bash')

        sys.path.insert( 0, os.path.join( os.getcwd(), 'modules') )
        sys.path.insert( 0, os.getcwd() )
        os.putenv("PYTHONPATH", ":".join(sys.path))

        self.cmd_targets, self.cmd_sources, self.basename = PySconsSetup(env=self.env)
        self.arguments = {}
        caller_frame = stack()[1]
        self.name = caller_frame[0].f_globals.get('__file__', None)
        self.current_procedure = self

    def expect_arguments(self, **args):
        for key in args:
            dtype, dval = args[key]

            if dtype is list:
                val = []
                i = 0
                while "%s_%d" % (key, i) in ARGUMENTS:
                    val.append(ARGUMENTS["%s_%d" % (key, i)])
                    i += 1
            else:
                val = ARGUMENTS.get(key, dval)
                if val is None:
                    raise AssertionError("Expected argument '%s' was not provided" % (key))
                val = dtype( val )

            self.arguments[key] = val

    def expect_sources(self, *names, **kwargs):
        extra_args = kwargs.get('extra', False)
        if extra_args:
            assert len(names) <= len(self.cmd_sources), "Number of sources does not match expectation %d > %d" % (len(self.cmd_sources), len(names))
        else:
            assert len(names) == len(self.cmd_sources), "Number of sources does not match expectation %d != %d" % (len(self.cmd_sources), len(names))

        for name, filepath in zip(names, self.cmd_sources):
            self.add_dependency( name, filepath )

        extras = []
        for i in xrange(len(names), len(self.cmd_sources)):
            k = 'extra_source_%d' % (i)
            extras.append(k)
            self.add_dependency( k, self.cmd_sources[i] )
        return extras

    def expect_targets(self, *names, **kwargs):
        extra_args = kwargs.get('extra', False)
        if extra_args:
            assert len(names) <= len(self.cmd_targets), "Number of targets does not match expectation %d > %d" % (len(self.cmd_targets), len(names))
        else:
            assert len(names) == len(self.cmd_targets), "Number of targets does not match expectation %d != %d" % (len(self.cmd_targets), len(names))

        for name, filepath in zip(names, self.cmd_targets):
            self.add_target( name, filepath )

        extras = []
        for i in xrange(len(names), len(self.cmd_targets)):
            k = 'extra_target_%d' % (i)
            extras.append(k)
            self.add_target( k, self.cmd_targets[i] )
        return extras

    def temp_target(self, name, path):
        return self.add_target( name, os.path.join(self.basename, path) )

class Procedure(object):
    def __init__( self, fn ):
        self.fn = fn
        self.name = fn.__name__
        self.children = []
        self.parents = []
        self.args = tuple([])
        self.kwargs = {}
        self.procedure_id = get_procedure_id()

    def __call__(self, *args, **kwargs):
        c = Procedure(self.fn)
        c.fn = self.fn
        c.name = self.fn.__name__
        c.args = args
        c.kwargs = kwargs
        c.procedure_id = get_procedure_id()
        return c

    def run( self, exp ):
        fn_args = tuple( [exp] + list(self.args) )
        self.fn( *fn_args, **self.kwargs )
        return SUCCESS_STATUS_CODE

    def add_parent(self, p):
        for t in p.tails():
            self.parents.append(t)

    def add_child(self, c):
        for h in c.heads():
            self.children.append(h)

    def heads(self):
        return [self]

    def tails(self):
        return [self]

    def __or__( self, proc ):
        return Chain( self, proc )

    def __add__( self, proc ):
        return Group( self, proc )

    def __repr__( self ):
        return """\
===Procedure===
ID:         %d
Name:       %s
*args:      %s
**kwargs:   %s""" % (self.procedure_id, self.name, str(self.args), str(self.kwargs))

class Chain(object):
    def __init__(self, *procs):
        self.procedures = list(procs)

        for p, c in zip(procs[:-1], procs[1:]):
            p.add_child(c)
            c.add_parent(p)

    def add_child(self, c):
        tail = self.procedures[-1]
        tail.add_child(c)

    def add_parent(self, p):
        head = self.procedures[0]
        head.add_parent(p)

    def heads(self):
        return self.procedures[0].heads()

    def tails(self):
        return self.procedures[-1].tails()

    def __or__ ( self, proc ):
        tail = self.procedures[-1]
        proc.add_parent( tail )
        tail.add_child( proc )
        self.procedures.append( proc )
        return self

    def __add__( self, proc ):
        return Group( self, proc )

class Group(object):
    def __init__(self, *procs):
        self.procedures = list(procs)

    def add_child(self, c):
        for p in self.procedures:
            p.add_child(c)

    def add_parent(self, p):
        for c in self.procedures:
            c.add_parent(p)

    def heads(self):
        return sum([ p.heads() for p in self.procedures ], [])

    def tails(self):
        return sum([ p.tails() for p in self.procedures ], [])

    def __or__ ( self, proc ):
        for p in self.procedures:
            proc.add_parent(p)
            p.add_child(proc)
        return Chain( self, proc )

    def __add__( self, proc ):
        self.procedures.append( proc )
        return self

class EndPoint(Procedure):
    def __init__( self, name ):
        Procedure.__init__( self, lambda exp: None )
        self.name = name

    def run( self, exp ):
        return SUCCESS_STATUS_CODE

    def __repr__( self ):
        return """==Endpoint: %s==""" % ( self.name )

class BreakPoint(Procedure):
    def __init__( self ):
        Procedure.__init__( self, lambda exp: None )
        self.name = "Breakpoint"

    def run( self, exp ):
        return BP_STATUS_CODE

    def __repr__( self ):
        return """==Breakpoint (%d)==""" % ( self.procedure_id )

def make_procedure( fn ):
    return Procedure( fn )

@make_procedure
def add_dependencies( exp, dependencies ):
    for name in dependencies:
        exp.add_dependency( name, dependencies[name])
