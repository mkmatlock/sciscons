// Create the renderer
var render = new dagreD3.render();

// Set up an SVG group so that we can translate the final graph.
var svg = d3.select("svg"),
inner = svg.append("g");

var zoom = d3.behavior.zoom().on("zoom", function() {
                      inner.attr("transform", "translate(" + d3.event.translate + ")" +
                          "scale(" + d3.event.scale + ")");
                      });
svg.call(zoom);

// Run the renderer. This is what draws the final graph.
render(d3.select("svg g"), g);

// Simple function to style the tooltip for the given node.
var styleTooltip = function(name, description) {
    return "<p class='name'>" + name + "</p><p class='description'>" + description + "</p>";
};

var clearHighlights = function() {
    d3.selectAll('div.file').classed('highlight', false);
    d3.selectAll('div.file').classed('highlight2', false);
    d3.selectAll('g.node').classed('highlight', false);
    d3.selectAll('g.node').classed('highlight2', false);
};


var setupNodeInfo = function(v) {
    d3.event.stopPropagation();
    clearHighlights();
    inner.selectAll("g.node").classed('selected', false);
    d3.select(g.node(v).elem).classed('selected', true);
    d3.select('div#info-dialog').style('visibility', "visible");

    d3.select('div#info-dialog span#name').text( g.node(v).name );
    d3.selectAll("div#build_dependencies div.sectionbody div").remove();
    for( var i in g.node(v).sources ) {
        var name = g.node(v).sources[i];
        d3.selectAll("div#build_dependencies div.sectionbody")
            .append( 'div' )
            .attr('class', "file dependency")
            .attr('title', file_sources[name])
            .html(name);
    }
    d3.selectAll("div#build_targets div.sectionbody div").remove();
    for( var i in g.node(v).targets ) {
        var name = g.node(v).targets[i];
        d3.selectAll("div#build_targets div.sectionbody")
            .append( 'div' )
            .attr('class', "file target")
            .attr('title', file_sources[name])
            .html(name);
    }
    d3.selectAll("div#build_steps div.sectionbody div").remove();
    for( var i in g.node(v).commands ) {
        d3.select("div#build_steps div.sectionbody")
            .append( 'div' )
            .attr( 'class', "command" )
            .html( g.node(v).commands[i] );
    }
    d3.selectAll("div#build_warnings div.sectionbody div").remove();
    for( var i in g.node(v).warnings ) {
        d3.select("div#build_warnings div.sectionbody")
            .append( 'div' )
            .attr( 'class', "warning" )
            .html( g.node(v).warnings[i] );
    }

    $("div.command div.flag").tipsy({ gravity:'s', trigger: 'hover', opacity: 1 });
      $('div.command code').each(function(i, block) {
              hljs.highlightBlock(block);
      });

    d3.selectAll("div.file.dependency")
        .on('click', function(e) {
            d3.event.stopPropagation();
            clearHighlights();
            var myNode = d3.select(this).classed( 'highlight', true );
            var fn = d3.select(this).text();

            d3.selectAll("div.file.dependency[data-name='"+fn+"']")
                .classed('highlight', true );
            d3.selectAll("div.file.target[data-name='"+fn+"']")
                .classed('highlight2', true );

            var v = file_node_sources[ fn ];
            d3.select(g.node(v).elem).classed( 'highlight', true );

            var fns = file_usages[ fn ];
            for( var i in fns ){
                var v2 = fns[i];
                d3.select(g.node(v2).elem).classed( 'highlight2', true );
            }
        });

    d3.selectAll("div.file.target")
        .on('click', function(e) {
            d3.event.stopPropagation();
            clearHighlights();

            var fn = d3.select(this).text();
            d3.selectAll("div.file.dependency[data-name='"+fn+"']")
                .classed('highlight', true );
            d3.selectAll("div.file.target[data-name='"+fn+"']")
                .classed('highlight2', true);

            d3.select(this).classed( 'highlight2', true );
            var fns = file_usages[ fn ];
            for( var i in fns ){
                var v = fns[i];
                d3.select(g.node(v).elem).classed( 'highlight2', true );
            }
        });

    $("div.file").tipsy({trigger:'hover', gravity:'w', opacity:1});
};
var hideNodeInfo = function() {
    d3.selectAll('g.node').classed('selected', false);
    d3.select('div#info-dialog').style('visibility', "hidden");
};

// Center the graph
var initialScale = 0.5;
zoom
    .translate([(svg.attr("width") - g.graph().width * initialScale) / 2, 20])
    .scale(initialScale)
    .event(svg);
var gHeight = g.graph().height * initialScale + 40;
var maxHeight = window.innerHeight - 200;
console.log(maxHeight);

if(maxHeight < gHeight){
    svg.attr('height', maxHeight);
}else{
    svg.attr('height', gHeight);
}

svg.on('click',
    function(e){
        if (d3.event.defaultPrevented) return;
        inner.selectAll("g.node").classed('selected', false);
        hideNodeInfo();
    });

d3.select("body")
    .on('click', function(e) {
        if (d3.event.defaultPrevented) return;
        clearHighlights();
    });

inner.selectAll("g.node")
    .attr("title", function(v) { return styleTooltip(g.node(v).name, g.node(v).description) })
    .on('click', setupNodeInfo)
    .each(function(v) {
            $(this).tipsy({ gravity: "w", opacity: 1, html: true, trigger: 'hover' });
        });
