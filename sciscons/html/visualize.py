import os, shutil
from collections import defaultdict
from pkg_resources import resource_filename

class DependencyAggregator():
    def __init__(self):
        self.node_info = {}
        self.node_children = {}
        self.node_commands = defaultdict( list )
        self.node_warnings = defaultdict( list )
        self.node_targets = defaultdict( list )
        self.node_ext_dependencies = defaultdict( list )
        self.node_new_dependencies = defaultdict( list )

        self.targets = {}
        self.dependencies = {}
        self.files = {}
        self.file_node_sources = {}
        self.file_usages = defaultdict( set )

    def run_procedure( self, procedure ):
        self.current_procedure = procedure

        procedure.run( self )
        self.post_process_node( procedure )

        self.current_procedure = None

    def post_process( self ):
        for pid in self.node_info:
            current_cids = self.node_children[pid]
            self.node_children[pid] = [ cid for cid in current_cids if cid in self.node_info ]

        for name in self.file_usages.keys():
            self.file_usages[name] = sorted( list( self.file_usages[name] ) )

    def post_process_node( self, procedure ):
        self.node_info[procedure.procedure_id] = {'type': procedure.__class__.__name__, 'name':procedure.name, 'repr':repr( procedure )}
        self.node_children[procedure.procedure_id] = [ p.procedure_id for p in procedure.children ]

        for cmd in self.node_commands[ procedure.procedure_id ]:
            for t in cmd['targets']:
                if t not in self.node_targets[ procedure.procedure_id ]:
                    self.node_targets[ procedure.procedure_id ].append( t )

        for cmd in self.node_commands[ procedure.procedure_id ]:
            for s in cmd['sources']:
                if s in self.node_targets[ procedure.procedure_id ]:
                    continue
                if s in self.node_new_dependencies[ procedure.procedure_id ]:
                    continue
                if s not in self.node_ext_dependencies[ procedure.procedure_id ]:
                    self.node_ext_dependencies[ procedure.procedure_id ].append( s )

        self.node_targets[ procedure.procedure_id ] = sorted( self.node_targets[ procedure.procedure_id ] )
        self.node_ext_dependencies[ procedure.procedure_id ] = sorted( self.node_ext_dependencies[ procedure.procedure_id ] )
        self.node_new_dependencies[ procedure.procedure_id ] = sorted( self.node_new_dependencies[ procedure.procedure_id ] )

    def add_target( self, name, path ):
        if path in self.files:
            name = self.files[path]
            pid = self.file_node_sources[name]
            self.node_warnings[self.current_procedure.procedure_id].append("Path '%s' already defined as '%s' in procedure %s (%d)" % (path, name, self.node_info[pid]['name'], pid))
        if name in self.targets:
            pid = self.file_node_sources[name]
            self.node_warnings[self.current_procedure.procedure_id].append("Target already defined with name '%s' in procedure %s (%d)" % (name, self.node_info[pid]['name'], pid))
        if name in self.dependencies:
            pid = self.file_node_sources[name]
            self.node_warnings[self.current_procedure.procedure_id].append("Dependency already defined with name '%s' in procedure %s (%d)" % (name, self.node_info[pid]['name'], pid))

        self.targets[name] = path
        self.dependencies[name] = path
        self.files[path] = name
        self.node_targets[ self.current_procedure.procedure_id ].append( name )
        self.file_node_sources[ name ] = self.current_procedure.procedure_id
        return name

    def add_dependency( self, name, path ):
        if path in self.files:
            name = self.files[path]
            pid = self.file_node_sources[name]
            self.node_warnings[self.current_procedure.procedure_id].append("Path '%s' already defined as '%s' in procedure %s (%d)" % (path, name, self.node_info[pid]['name'], pid))
        if name in self.targets:
            pid = self.file_node_sources[name]
            self.node_warnings[self.current_procedure.procedure_id].append("Target already defined with name '%s' in procedure %s (%d)" % (name, self.node_info[pid]['name'], pid))
        if name in self.dependencies:
            pid = self.file_node_sources[name]
            self.node_warnings[self.current_procedure.procedure_id].append("Dependency already defined with name '%s' in procedure %s (%d)" % (name, self.node_info[pid]['name'], pid))

        self.dependencies[name] = path
        self.files[path] = name
        self.node_new_dependencies[ self.current_procedure.procedure_id ].append( name )
        self.file_node_sources[ name ] = self.current_procedure.procedure_id
        return name

    def __process_sources( self, sources ):
        if isinstance(sources, tuple):
            sources = list(sources)
        if not isinstance(sources, list):
            sources = [ sources ]

        for name in sources:
            self.file_usages[ name ].add( self.current_procedure.procedure_id )

        for name in sources:
            if name not in self.targets and name not in self.dependencies:
                self.node_warnings[self.current_procedure.procedure_id].append("Cannot locate dependency: No such source or target named '%s'" % (name))
        return sources

    def __process_targets( self, targets ):
        if isinstance(targets, tuple):
            targets = list(targets)
        if not isinstance(targets, list):
            targets = [ targets ]

        for name in targets:
            if name not in self.targets:
                self.node_warnings[self.current_procedure.procedure_id].append("Cannot locate target: No such target named '%s'" % (name))
        return targets

    def shell( self, targets, sources, command, script=None, shell=False, exit=False ):
        sources = self.__process_sources( sources )
        targets = self.__process_targets( targets )

        self.node_commands[ self.current_procedure.procedure_id ].append(
                { 'type': 'shell', 'targets':targets, 'sources':sources, 'command': command,
                    'kwargs':{'script': script, 'shell': shell, 'exit': exit } } )

    def python( self, targets, sources, module, args=None, script=False, shell=False, exit=False ):
        sources = self.__process_sources( sources )
        targets = self.__process_targets( targets )

        if args is None:
            args = "$SOURCES > $TARGET"
            if script:
                args = "${SOURCES[1:]} > $TARGET"

        self.node_commands[ self.current_procedure.procedure_id ].append(
                { 'type': 'python', 'targets':targets, 'sources':sources, 'module': module,
                    'kwargs':{'args': args, 'script': script, 'shell': shell, 'exit': exit } } )

    def xval( self, targets, sources, command, options="-Q", sig_options="", j=4, folds=10, seed=31223, groups=True, exit=False, tempdir=None, clean=True):
        sig_opts = "command='%s' options='%s' sig_options='%s' ext=%d seed=%d folds=%d clean=%s groups=%s" % (command, options, sig_options, 1 if exit else 0, seed, folds, str(clean), str(groups))

        self.scons( targets, sources, resource_filename('sciscons', 'xval.scons'),
                    j=j, sig_options=sig_opts, tempdir=tempdir, exit=False, clean=clean )

    def multiprocess( self, targets, source_table, extra_sources, command, N, j=4, tempdir=None, shell=False, exit=False, clean=True ):
        self.scons( targets, [source_table] + extra_sources, resource_filename('sciscons', 'multiprocess.scons'),
                 j=j, sig_options='command="%s" N=%d' % (command, N),
                 exit=exit, clean=clean, tempdir=tempdir )

    def scons( self, targets, sources, scons_file, options="-Q", sig_options="", j=4, tempdir=None, exit=False, clean=True ):
        sources = self.__process_sources( sources )
        targets = self.__process_targets( targets )

        options += " -j %d" % (j)
        self.node_commands[ self.current_procedure.procedure_id ].append(
                { 'type': 'scons', 'targets':targets, 'sources':sources, 'script': scons_file,
                    'kwargs':{'options':options, 'sig_options':sig_options, 'tempdir':tempdir, 'exit':exit, 'clean':clean} } )

    def perform_target_access( self, names, access_func, default_value ):
        return default_value

def get_dep_html( name, tp, filename=None ):
    if filename is None:
        return """<div class="file %s" data-name="%s" >%s</div>""" % (tp, name, name)
    else:
        return """<div class="file source %s" data-name="%s" title="%s" >%s</div>""" % (tp, name, filename, name)

def make_flag_html( flag, description ):
    return """<div class="flag" title="%s">%s</div>""" % (description, flag)

def format_html( text ):
    return text.replace(">", "&#62;").replace("<", "&#60;")

def highlight( code ):
    return """<code class="bash">%s</code>""" % (code)

def format_command( command ):
    flag_html = []
    if command['kwargs']['exit']:
        flag_html.append( make_flag_html('R', "Run remote") )
    flag_html = "".join(flag_html)

    source_html = []
    for source in command['sources']:
        source_html.append( get_dep_html(source, 'dependency') )
    target_html = []
    for target in command['targets']:
        target_html.append( get_dep_html(target, 'target') )
    source_html = "".join(source_html)
    target_html = "".join(target_html)

    if command['type'] == 'scons':
        if command['kwargs']['clean']:
            flag_html += make_flag_html( 'C', 'Clean Intermediate Files' )
        command_text = "scons %s %s %s tempdir=%s" % (command['script'], command['kwargs']['options'], command['kwargs']['sig_options'], str(command['kwargs']['tempdir']))

    if command['type'] == 'python':
        if command['kwargs']['script']:
            flag_html += make_flag_html( 'T', 'Track python script' )
        if command['kwargs']['shell']:
            flag_html += make_flag_html( 'S', 'Run in Shell' )
        command_text = "python -m %s %s" % (command['module'], command['kwargs']['args'])

    if command['type'] == 'shell':
        if command['kwargs']['script'] is not None:
            flag_html += make_flag_html( 'T', 'Track shell script: %s' % (command['kwargs']['script']) )
        if command['kwargs']['shell']:
            flag_html += make_flag_html( 'S', 'Run in Shell' )
        command_text = "%s" % (command['command'])

    return """<div class="comblock flagblock">%s</div><div class="comblock">%s</div><div class="comblock">&#8594;</div><div class="comblock">%s</div><div class="comblock">&#8594;</div><div class="comblock">%s</div>""" % (flag_html, source_html, highlight(format_html(command_text)), target_html)

def report_errors( stream, G, project_name ):
    if sum([ len(G.node_warnings[pid]) for pid in G.node_warnings ]) == 0:
        print >>stream, "No errors or warnings found for project:", project_name
    for pid in G.node_warnings:
        print >>stream, "------------------"
        print >>stream, "Errors in process:"
        label = "%s (%d)" % (G.node_info[ pid ][ 'name' ], pid)
        cls = G.node_info[ pid ][ 'type' ]
        print >>stream, label
        print >>stream, cls
        print >>stream, "------------------"
        print >>stream, "Error list:"
        print >>stream, "------------------"
        for error in G.node_warnings[pid]:
            print >>stream, error
        print >>stream, "------------------"
        print >>stream, ""

def write_html( G, project_name, html_file ):
    DESC_TEMPLATE = "Imports: %d<br/> Dependencies: %d<br/>Targets: %d<br/>Steps: %d<br/>Warnings: %d<br/>"
    EDGE_COMMAND_TEMPLATE = """g.setEdge( %d, %d, {lineInterpolate: 'basis'} ); """
    NODE_COMMAND_TEMPLATE = """g.setNode( %d, {label: "%s", class:"%s", name:"%s", description:"%s", sources:%s, targets:%s, commands:%s, warnings:%s} ); """

    NODE_COMMANDS = []
    EDGE_COMMANDS = []

    for pid in G.node_info:
        label = "%s (%d)" % (G.node_info[ pid ][ 'name' ], pid)
        cls = G.node_info[ pid ][ 'type' ]
        desc = DESC_TEMPLATE % (len(G.node_new_dependencies[pid]), len(G.node_ext_dependencies[pid]), len(G.node_targets[pid]), len(G.node_commands[pid]), len(G.node_warnings[pid]))
        dep_str = str( G.node_new_dependencies[pid] + G.node_ext_dependencies[pid] )
        tar_str = str( G.node_targets[pid] )
        com_str = str( [format_command(c) for c in G.node_commands[pid]] )
        warn_str = str( G.node_warnings[pid] )
        if len( G.node_warnings[pid] ) > 0:
            cls += " warning"

        node_cmd = NODE_COMMAND_TEMPLATE % ( pid, label, cls, label, desc, dep_str, tar_str, com_str, warn_str )
        NODE_COMMANDS.append( node_cmd )

        for cid in G.node_children[pid]:
            edge_cmd = EDGE_COMMAND_TEMPLATE % (pid, cid)
            EDGE_COMMANDS.append( edge_cmd )

    FILE_NODE_SOURCES = ""
    for name in G.file_node_sources:
        FILE_NODE_SOURCES += "'%s':%d,\n" % (name, G.file_node_sources[name])

    FILE_SOURCES = ""
    for name in G.dependencies:
        FILE_SOURCES += "'%s':'%s',\n" % (name, G.dependencies[name])
    for name in G.targets:
        FILE_SOURCES += "'%s':'%s',\n" % (name, G.targets[name])

    FILE_USAGES = ""
    for name in G.file_usages:
        FILE_USAGES += "'%s': [ %s ],\n" % ( name, ", ".join( str(pid) for pid in G.file_usages[name] ) )

    shutil.copy( resource_filename('sciscons.html', 'tipsy.js'), os.path.join( os.path.dirname(html_file), 'tipsy.js' ) )
    shutil.copy( resource_filename('sciscons.html', 'tipsy.css'), os.path.join( os.path.dirname(html_file), 'tipsy.css' ) )
    shutil.copy( resource_filename('sciscons.html', 'sciscons.js'), os.path.join( os.path.dirname(html_file), 'sciscons.js' ) )
    shutil.copy( resource_filename('sciscons.html', 'sciscons.css'), os.path.join( os.path.dirname(html_file), 'sciscons.css' ) )

    with open(resource_filename('sciscons.html', 'template.html'), 'r') as htmltemplate_file:
        html_template = htmltemplate_file.read()

    html_template = html_template.replace("PROJECT_NAME", project_name)
    html_template = html_template.replace("FILE_NODE_SOURCES", FILE_NODE_SOURCES)
    html_template = html_template.replace("FILE_SOURCES", FILE_SOURCES)
    html_template = html_template.replace("FILE_USAGES", FILE_USAGES)
    html_template = html_template.replace("NODE_COMMANDS", "\n".join(['            %s' % (nc) for nc in NODE_COMMANDS]))
    html_template = html_template.replace("EDGE_COMMANDS", "\n".join(['            %s' % (ec) for ec in EDGE_COMMANDS]))

    with open(html_file, 'w') as outfile:
        outfile.write( html_template )
