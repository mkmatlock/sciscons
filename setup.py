from distutils.core import setup

setup(name='SciScons',
        version='1.0',
        description='SciScons: tools for semantic organization of scons build files for scientific experiments',
        author='Matt Matlock',
        author_email='matt.matlock@gmail.com',
      install_requires=['argparse>=1.4.0',
                        'argcomplete>=1.4.1',
                        'pyscons>=1.0.87'],
        url='http://swami.wustl.edu',
        packages=['sciscons', 'sciscons.default_project', 'sciscons.html'],
        package_dir={'sciscons.default_project': 'sciscons/default_project',
                     'sciscons.html': 'sciscons/html'},
        package_data={'sciscons': ['*.scons'],
                      'sciscons.default_project':
                                   ['.hgignore',
                                    'rscons.config',
                                    'setup.sci',
                                    'SConstruct'],
                      'sciscons.html':
                                   ['sciscons.js',
                                    'sciscons.css',
                                    'template.html',
                                    'tipsy.js',
                                    'tipsy.css']},
        scripts=['scripts/sciscons']
        )
